# CLI Conan Package Script

This repo contains a Conan package script for the CLI library.
Currently only version 1.1.1 of CLI library is supported.

# CLI library

The CLI library is a library for interactive command line interfaces
in modern C++.

For more info see:
https://github.com/daniele77/cli

