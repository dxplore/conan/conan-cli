#include <cli/cli.h>
#include <cli/cliasyncsession.h>

int main()
{
	auto rootMenu = std::make_unique<cli::Menu>("test");
	cli::Cli cli(std::move(rootMenu));
	boost::asio::io_context ios;
	cli::CliAsyncSession session(ios, cli);
	return 0;
}
