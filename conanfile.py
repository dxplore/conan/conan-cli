from conans import ConanFile, tools


class CliConan(ConanFile):
    name = "cli"
    version = "1.1.1"
    description = """A library for interactive command line interfaces in
                     modern C++"""
    homepage = "https://github.com/daniele77/cli"
    url = "https://gitlab.com/dxplore/conan-cli"
    license = "BSL-1.0"
    author = "Jan Van Winkel <jan.van_winkel@dxplore.eu>"
    topics = ("cli", "cisco")
    no_copy_source = True
    # No settings/options are necessary, this is header only
    requires = "boost/1.72.0"

    def source(self):
        url = "https://github.com/daniele77/cli/archive/"
        url += "v%s.tar.gz" % self.version
        tools.get(url=url)

    def package(self):
        self.copy("*.h", dst="include", src="cli-%s/include" % self.version)
